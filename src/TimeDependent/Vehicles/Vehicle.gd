extends Area2D

export (float) var speed = 500

var left_limit = 0
var right_limit = 1600
var right2left = false
var time_flow = 1.0
var state = 0
enum {INACTIVE, ACTIVE}

func _ready() -> void:
    change_state(INACTIVE)
        
func _process(delta) -> void:
    if state == ACTIVE:
        if right2left:
            position.x -= delta * speed * time_flow
        else:
            position.x += delta * speed * time_flow
        if position.x > right_limit:
            position.x = left_limit + position.x - right_limit
        if position.x < left_limit:
            position.x = right_limit + position.x - left_limit
                        
func start(pos, _left_limit, _right_limit, _right2left, _time_flow) -> void:
    position = pos
    left_limit = _left_limit
    right_limit = _right_limit
    right2left = _right2left
    if right2left:
        set_rotation_degrees(180)
    time_flow = _time_flow
    $ExhaustPlayer.set_speed_scale(time_flow) 
    change_state(ACTIVE)
    $ExhaustPlayer.play("smoke")
    
func change_state(new_state) -> void:
    match new_state:
        # collision off
        INACTIVE:
            $CollisionShape2D.set_deferred("disabled", true)
        # collision on
        ACTIVE:
            $CollisionShape2D.set_deferred("disabled", false)
    state = new_state

func _on_time_flow_changed(new_time_flow):
    time_flow = new_time_flow
    $ExhaustPlayer.set_speed_scale(time_flow) 
