extends Control

func _ready() -> void:
    $Menu/ContinueButton.connect("pressed", self, "_on_continue_button_pressed")
    $Menu/ToMenuButton.connect("pressed", self, "_on_to_menu_button_pressed")

func _input(event: InputEvent) -> void:
    if event.is_action_pressed("pause"):
        var is_paused = not get_tree().paused
        get_tree().paused = is_paused
        visible = is_paused

func _on_to_menu_button_pressed() -> void:
    get_tree().change_scene("res://src/GUI/TitleScreen.tscn")
    get_tree().paused = false
    
func _on_continue_button_pressed() -> void:
    var is_paused = not get_tree().paused
    get_tree().paused = is_paused
    visible = is_paused
