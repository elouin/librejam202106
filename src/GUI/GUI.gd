extends CanvasLayer

onready var scene_tree: = get_tree()
onready var FrogCounter: Label = get_node("VBoxContainer/FrogCounter")
onready var ScoreCounter: Label = get_node("VBoxContainer/ScoreCounter")

var paused: = false setget set_paused

func _ready():
    $PauseMenu/Menu/ContinueButton.connect("pressed", self, "_on_continue_button_pressed")
    $PauseMenu/Menu/ToMenuButton.connect("pressed", self, "_on_to_menu_button_pressed")
    

func _on_frogs_changed(frogs_left) -> void:
    FrogCounter.text = "Frogs: " + str(frogs_left)

func _on_score_changed(new_score) -> void:
    ScoreCounter.text = "Score: " + str(new_score)

func _on_pause_button_pressed() -> void:
    self.paused = !paused

func set_paused(value: bool) -> void:
    paused = value
    scene_tree.paused = value
    $PauseMenu.visible = value
    $PauseButton.visible = !value
    $PauseMenu/Menu/Label.set_text("Level: "+str(PlayerData.level_nr))

func _input(event: InputEvent) -> void:
    if event.is_action_pressed("pause"):
        self.paused = !paused

func _on_to_menu_button_pressed() -> void:
    scene_tree.change_scene("res://src/GUI/TitleScreen.tscn")
    scene_tree.paused = false
    
func _on_continue_button_pressed() -> void:
    self.paused = !paused
