extends Control

export (PackedScene) var HighscoreEntryLabelScene

func _ready() -> void:
    $Menu/Center/Buttons/NewGameButton.connect("pressed", self, "_on_Button_pressed", [$Menu/Center/Buttons/NewGameButton.target_scene])
    $Menu/Center/Buttons/QuitButton.connect("pressed", self, "_on_Quit_Button_pressed")
    PlayerData.level_nr = 1
    var entry = HighscoreEntryLabelScene.instance()
    $Highscores.add_child(entry)
    entry.set_text("Level: Highscore")
    for lvl in PlayerData.highscores:
        entry = HighscoreEntryLabelScene.instance()
        $Highscores.add_child(entry)
        entry.set_text(str(lvl)+": "+str(PlayerData.highscores[lvl]))

func _on_Button_pressed(target_scene):
    get_tree().change_scene(target_scene)
    
func _on_Quit_Button_pressed():
    get_tree().quit()
