extends Node

export (PackedScene) var CarScene
export (PackedScene) var TruckScene
export (PackedScene) var FrogScene
export (PackedScene) var StreetScene
export (PackedScene) var RaindropScene

signal score_changed
signal frogs_changed

var vehicles = []
var score = 0 setget _set_score
var frogs = 0 setget _set_frogs
var frog_lane_order = []
var frog_lane_order_i = 0
var current_level_nr = 1

func _ready():
    $GUI.paused = true
    $GUI/TimeSlider.connect('value_changed', self, '_on_time_flow_changed')
    self.connect('frogs_changed', $GUI, '_on_frogs_changed')
    self.connect('score_changed', $GUI, '_on_score_changed')
    _spawn_rain()
    start_level(1)
    yield(get_tree().create_timer(4), "timeout")
    $GUI/TimeSlider.visible=true

func _spawn_rain():
    for _i in range(500):
        var drop = RaindropScene.instance()
        drop.position = Vector2(randi()%1600,randi()%1600)
        drop.rotate(-0.1 + 0.3*randf())
        $Raindrops.add_child(drop)
        drop.start($GUI/TimeSlider.value)
        $GUI/TimeSlider.connect('value_changed', drop, '_on_time_flow_changed')

func start_level(level_nr):
    PlayerData.level_nr = level_nr
    seed(level_nr+1)
    vehicles = [CarScene, TruckScene]
    #
    self.score += 0
    #
    self.frogs = level_nr
    spawn_vehicles(calculate_vehicle_positions(9), 350) 
    #
    if level_nr > 9:
        self.frogs = level_nr - 7
        spawn_vehicles(calculate_vehicle_positions(10), 120) 
    #
    if level_nr > 19:
        self.frogs = level_nr - 17
        spawn_vehicles(calculate_vehicle_positions(11), 580) 
    #
    if self.frogs > 30:
         self.frogs = 30
    #
    _calculate_frog_lane_order()
    for _i in range(self.frogs):
        spawn_frog()

func _process(_delta):
    if $StreetNoise.playing == false:
        $StreetNoise.play()

func _calculate_frog_lane_order():
    frog_lane_order = []
    for i in range(30):
        frog_lane_order.append(i)
    frog_lane_order.shuffle()

func _get_next_lane():
    frog_lane_order_i += 1
    if frog_lane_order_i >= len(frog_lane_order):
        frog_lane_order_i = 0
    return 230 + frog_lane_order[frog_lane_order_i]*40

func spawn_frog():
    var frog = FrogScene.instance()
    #$Frogs.call_deferred("add_child",frog)
    $Frogs.add_child(frog)
    frog.scale = Vector2(0.08,0.08)
    frog.get_node("Body").set_modulate(Color(0.8*randf(),0.5+0.5*randf(),0.8*randf(),1 ))
    frog.connect('frog_hit', self, '_on_frog_hit')
    frog.connect('frog_finished', self, '_on_frog_finished')
    frog.start(Vector2(_get_next_lane(), 1000 + randf() * 600))

func spawn_vehicles(vehicle_positions:Array, y_pos):
    var street = StreetScene.instance()
    street.set_scale(Vector2(2.1,2))
    $Decorations.add_child(street)
    street.set_position(Vector2(800, y_pos))
    for y_pos_i_r2l in [[y_pos + 40, false],[y_pos - 40, true]]:
        for x_pos in vehicle_positions:
            var vehicle = vehicles[randi()%len(vehicles)].instance()
            vehicle.scale = Vector2(0.1,0.1)
            vehicle.get_node("Body").set_modulate(Color(0.2+0.6*randf(),0.2+0.6*randf(),0.2+0.6*randf(),1 ))
            $Vehicles.add_child(vehicle)
            $GUI/TimeSlider.connect('value_changed', vehicle, '_on_time_flow_changed')
            vehicle.start(
                Vector2(x_pos, y_pos_i_r2l[0]), 
                vehicle_positions.min(), 
                vehicle_positions.max()+350, 
                y_pos_i_r2l[1], 
                $GUI/TimeSlider.value)

func calculate_vehicle_positions(number) -> Array:
    var positions = [-1000]
    for i in range(number):
        positions.append(positions[i] + 200 + (randi()%500))
    return positions
    
func _on_frog_hit():
    self.score -= 1
    spawn_frog()
    
func _on_frog_finished():
    self.frogs -= 1
    self.score += 10
    if self.frogs <= 0:
        win()
    
func win():
    yield(get_tree().create_timer(1), "timeout")
    for vehicle in $Vehicles.get_children():
        vehicle.queue_free()
    if not PlayerData.level_nr in PlayerData.highscores:
        PlayerData.highscores[PlayerData.level_nr] = self.score
        PlayerData.write_highscores()
    elif PlayerData.highscores[PlayerData.level_nr] < self.score:
        PlayerData.highscores[PlayerData.level_nr] = self.score
        PlayerData.write_highscores()
    PlayerData.level_nr += 1
    start_level(PlayerData.level_nr)
    $GUI.paused = true

func _set_score(new_score):
    emit_signal("score_changed", new_score)
    score = new_score

func _set_frogs(new_frogs):
    emit_signal("frogs_changed", new_frogs)
    frogs = new_frogs
    
func _on_time_flow_changed(time_flow):
    if time_flow>0:
        $StreetNoise.volume_db = 0
        $StreetNoiseRev.volume_db = -80
        $StreetNoise.pitch_scale = time_flow
        $Rain.volume_db = 0
        $RainRev.volume_db = -80
        $Rain.pitch_scale = time_flow
    elif time_flow<0:
        $StreetNoise.volume_db = -80
        $StreetNoiseRev.volume_db = 0
        $StreetNoiseRev.pitch_scale = -time_flow
        $Rain.volume_db = -80
        $RainRev.volume_db = 0
        $RainRev.pitch_scale = -time_flow
    else:
        $StreetNoise.volume_db = 0
        $StreetNoiseRev.volume_db = 0
        $Rain.volume_db = 0
        $RainRev.volume_db = 0
        
